local set = vim.opt
vim.cmd [[colorscheme catppuccin]]
vim.cmd [[set fcs=eob:\ ]]
set.tabstop = 2
set.shiftwidth = 2
set.softtabstop = 2
set.expandtab = false

set.number = true
set.mouse = 'a'
set.clipboard = 'unnamedplus'

